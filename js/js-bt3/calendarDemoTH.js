angular.module('calendarDemoApp', ['ui.rCalendar']);

angular.module('calendarDemoApp').controller('CalendarDemoCtrl', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "",
                noidung: "Công bố công khai phê duyệt điều chỉnh quy định bảo vệ tài nguyên môi trường đến năm 2020",
                thamdu: "Chi cục trưởng, Chi cục phó, Chuyên viên",
                diadiem: "17 Trung Yên 3, Trung Hòa, Cầu Giấy",
                startTime: new Date(2019, 7, 23, 8, 0, 0, 0),
                endTime: new Date(2019, 7, 23, 11, 59, 59, 0),
                allDay: false
            },
            {
                title: "",
                noidung: "Công bố công khai Kế hoạch của chi cục bảo vệ môi trường Hà Nội năm 2020",
                thamdu: "Chi cục trưởng, Chi cục phó, Chuyên viên",
                diadiem: "17 Trung Yên 3, Trung Hòa, Cầu Giấy",
                startTime: new Date(2019, 7, 23, 13, 0, 0, 0),
                endTime: new Date(2019, 7, 23, 23, 59, 59, 0),
                allDay: false
            },
            {
                title: "",
                noidung: "Công bố kết quả thống kê doanh nghiệp bảo vệ môi trường thành phố Hà Nội năm 2019",
                thamdu: "Chi cục trưởng, Chi cục phó, Chuyên viên",
                diadiem: "17 Trung Yên 3, Trung Hòa, Cầu Giấy",
                startTime: new Date(2019, 7, 25, 15, 0, 0, 0),
                endTime: new Date(2019, 7, 25, 17, 59, 59, 0),
                allDay: false
            }
           
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

// end bộ trưởng  ----->

angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr1', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };
console.log(123);
    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "",
                noidung: "Công bố công khai phê duyệt điều chỉnh quy hoạch sử dụng đất đến năm 2020",
                thamdu: "Giám đốc sở, P Giám đốc sở, Chuyên viên",
                diadiem: "18 Huỳnh Thúc Kháng",
                startTime: new Date(2019, 7, 17, 8, 0, 0, 0),
                endTime: new Date(2019, 7, 17, 11, 59, 59, 0),
                allDay: false
            },
            {
                title: "",
                noidung: "Công bố công khai Kế hoạch sử dụng đất năm 2019 của các quận, huyện",
                thamdu: "Giám đốc sở, P Giám đốc sở, Chuyên viên",
                diadiem: "18 Huỳnh Thúc Kháng ",
                startTime: new Date(2019, 7, 17, 13, 0, 0, 0),
                endTime: new Date(2019, 7, 17, 17, 59, 59, 0),
                allDay: false
            },
            {
                title: "",
                noidung: "Công bố kết quả thống kê diện tích đất đai thành phố Hà Nội năm 2019",
                thamdu: "Giám đốc sở, P Giám đốc sở, Chuyên viên",
                diadiem: "18 Huỳnh Thúc Kháng ",
                startTime: new Date(2019, 7, 18, 13, 0, 0, 0),
                endTime: new Date(2019, 7, 18, 17, 59, 59, 0),
                allDay: false
            },
            {
                title: "",
                noidung: "Công bố kết quả thống kê diện tích đất đai thành phố Hà Nội năm 2019",
                thamdu: "Giám đốc sở, P Giám đốc sở, Chuyên viên",
                diadiem: "18 Huỳnh Thúc Kháng ",
                startTime: new Date(2019, 7, 18, 8, 0, 0, 0),
                endTime: new Date(2019, 7, 18, 11, 59, 59, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

// end thứ trưởng 1  ----->
angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr2', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Theo dõi, ứng phó với siêu bão MangKhut",
                noidung: "Theo dõi, ứng phó với siêu bão MangKhut",
                thamdu: "",
                diadiem: "",
                startTime: new Date(2018, 9, 23, 8, 0, 0, 0),
                endTime: new Date(2018, 9, 23, 23, 59, 0, 0),
                allDay: false
            },
            {
                title: "Dự Lễ khai giảng năm học mới Trường Đại học Thủy lợi",
                noidung: "Dự Lễ khai giảng năm học mới Trường Đại học Thủy lợi ",
                thamdu: "Theo giấy mời,  ",
                diadiem: "Theo giấy mời,  ",
                startTime: new Date(2018, 9, 24, 8, 0, 0, 0),
                endTime: new Date(2018, 9, 24, 8, 30, 0, 0),
                allDay: false
            },
            {
                title: " Họp Chính phủ về các vướng mắc trong việc chuẩn bị các chương trình, dự án vay vốn COL (ADF) của Ngân hàng Phát triển Châu á (ADB) tài khóa 2018 ",
                noidung: " Họp Chính phủ về các vướng mắc trong việc chuẩn bị các chương trình, dự án vay vốn COL (ADF) của Ngân hàng Phát triển Châu á (ADB) tài khóa 2018 ",
                thamdu: "Vụ HTQT chuẩn bị và cùng dự,  ",
                diadiem: "VPCP ",
                startTime: new Date(2018, 9, 25, 10, 0, 0, 0),
                endTime: new Date(2018, 9, 25, 11, 30, 0, 0),
                allDay: false
            },
            {
                title: "Nghe báo cáo TKKT dự án tiêu úng vùng III Nông Cống, tỉnh Thanh Hóa ",
                noidung: "Nghe báo cáo TKKT dự án tiêu úng vùng III Nông Cống, tỉnh Thanh Hóa ",
                thamdu: "Vụ trưởng Vụ TCCB chuẩn bị và cùng dự.",
                diadiem: "Phòng khách A604",
                startTime: new Date(2018, 9, 26, 15, 0, 0, 0),
                endTime: new Date(2018, 9, 26, 16, 0, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

// end thứ trưởng 2  ----->
angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr3', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Đi công tác nước ngoài ",
                noidung: "Đi công tác nước ngoài ",
                thamdu: "Vụ HTQT*, Viện CSCLNN&PTNT.,  ",
                diadiem: "Anh",
                startTime: new Date(2018, 9, 23, 8, 0, 0, 0),
                endTime: new Date(2018, 9, 23, 23, 59, 59, 0),
                allDay: false
            },
            {
                title: "Đi công tác nước ngoài ",
                noidung: "Đi công tác nước ngoài ",
                thamdu: "Vụ HTQT*, Viện CSCLNN&PTNT.,  ",
                diadiem: "Anh",
                startTime: new Date(2018, 9, 24, 0, 1, 0, 0),
                endTime: new Date(2018, 9, 24, 23, 59, 0, 0),
                allDay: false
            },
            {
                title: "Đi công tác nước ngoài ",
                noidung: "Đi công tác nước ngoài ",
                thamdu: "Vụ HTQT*, Viện CSCLNN&PTNT.,  ",
                diadiem: "Anh",
                startTime: new Date(2018, 9, 25, 0, 1, 0, 0),
                endTime: new Date(2018, 9, 25, 23, 59, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);


// end thứ trưởng 3  ----->
angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr4', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
            title: " Họp công tác chuẩn bị hội nghị ổn định dân di cư tự do ",
            noidung: " Họp công tác chuẩn bị hội nghị ổn định dân di cư tự do ",
            thamdu: "Cục KTHT,  ",
            diadiem: "218-A10 ",
            startTime: new Date(2018, 8, 22, 8, 30, 0, 0),
            endTime: new Date(2018, 8, 22, 9, 45, 0, 0),
            allDay: false
        },
        {
            title: "Làm việc với Ban chủ nhiệm Chương trình KHCN NTM ",
            noidung: "Làm việc với Ban chủ nhiệm Chương trình KHCN NTM ",
            thamdu: "VPĐP*,  ",
            diadiem: "102-B6 ",
            startTime: new Date(2018, 8, 22, 14, 0, 0, 0),
            endTime: new Date(2018, 8, 22, 14, 30, 0, 0),
            allDay: false
        },
        {
            title: "Hội nghị trực tuyến Chính phủ Tổng kết Phong trào Toàn dân đoàn kết xây dựng đời sống văn hóa ",
            noidung: "Hội nghị trực tuyến Chính phủ Tổng kết Phong trào Toàn dân đoàn kết xây dựng đời sống văn hóa ",
            thamdu: "VPĐP,  ",
            diadiem: "11 Lê Hồng Phong ",
            startTime: new Date(2018, 8, 22, 8, 0, 0, 0),
            endTime: new Date(2018, 8, 22, 11, 0, 0, 0),
            allDay: false
        },
        {
            title: "Làm việc Tổ chức IFOAM, Hiệp hội nông nghiệp hữu cơ ",
            noidung: "Làm việc Tổ chức IFOAM, Hiệp hội nông nghiệp hữu cơ ",
            thamdu: " ",
            diadiem: "218-A10 ",
            startTime: new Date(2018, 8, 22, 14, 30, 0, 0),
            endTime: new Date(2018, 8, 22, 15, 0, 0, 0),
            allDay: false
        },];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);